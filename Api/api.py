import requests

# Replace YOUR_PRIVATE_TOKEN with your own GitLab private token
private_token = "glpat-3K2yarp3wxzjxQJP4ak-"
url = "https://gitlab.com/api/v4/users/FastFahrul/projects"

headers = {
    "Private-Token": private_token
}

response = requests.get(url, headers=headers)

if response.status_code == 200:
    projects = response.json()
    for project in projects:
        print("Project name:", project["name"])
        print("Project URL:", project["web_url"])
        print("---")
else:
    print("Failed to retrieve projects. HTTP status code:", response.status_code)
